# -*- coding: utf-8 -*-

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="upm",
    scripts=['upm/bin/upm', 'upm/bin/nuttx'],
    version="0.5.0",
    author="Maciej Wójcik",
    author_email="w8jcik@gmail.com",
    description="Unofficial, opinionated and Linux-only NuttX project management tool",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/w8jcik/upm",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ],
    package_data={
        "upm": ['lib/tools/*.sh', 'lib/tools/*.bash', 'tests/*.sh']
    },
    include_package_data=True,
    install_requires=[
        'readchar>=2.0',
    ],
)
