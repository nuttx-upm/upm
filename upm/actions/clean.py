import subprocess
from upm.lib.Script import Script
script = Script()


def clean(repo, args):

    if args.apps:
        try:
            res_code = subprocess.call(["bash", "-c", "find . -type f -name '*.o' ; find . -type f -name '*.a'"], cwd="%s/apps" % repo.location)
            if res_code != 0:
                exit(script.prefix("cleaning preparation failed!"))
        except (KeyboardInterrupt, ProcessLookupError):
            print()
            exit(script.prefix("cleaning preparation interrupted!"))

        try:
            res_code = subprocess.call(["bash", "-c", "find . -type f -name '*.o' -delete ; find . -type f -name '*.a' -delete"], cwd="%s/apps" % repo.location)
            if res_code != 0:
                exit(script.prefix("cleaning failed!"))
        except (KeyboardInterrupt, ProcessLookupError):
            print()
            exit(script.prefix("cleaning interrupted!"))

        exit()

    if len(repo.oses) > 0:
        if repo.active_os == None:
            repo.detect_issues()
        else:
            repo.save_downloaded()
            repo.clean()
            repo.restore_downloaded()
