import importlib.util
import os, subprocess, shutil

from upm.lib.Script import Script
script = Script()


def config(repo, args):

    if args.link_apps:
        repo.link_external_apps()
        exit()

    if args.startup:
        # TODO:
        # * check for existence of link and configuration options
        # * depending on configuration change location of romfsimg or warn if not configured
        # * git should ignore resulting file and the file should be regenerated when changed
        # * file should be removed when unconfigured (a proposition)
        # * previous command: cd active/os/apps/nshlib ; nano rcS.template ; ../../nuttx/tools/mkromfsimg.sh ~/scr/uc/active/os/nuttx

        if not os.path.exists("%s/active/os/board" % repo.location):
            print('link to active board is missing');
            exit()

        if not args.no_confirm:
            subprocess.call("nano rcS.template", cwd="%s/active/os/board/include" % repo.location, shell=True)
        subprocess.call("%s/active/os/nuttx/tools/mkromfsimg.sh %s/active/os/nuttx" % (repo.location, repo.location), cwd="%s/active/os/board/include" % repo.location, shell=True)
        exit()

    working_config_location = "%s/active/os/nuttx/.config" % repo.location
    generated_config_location = "%s/active/os/nuttx/.config~" % repo.location
    selected_config_location = "%s/active/os/config/defconfig" % repo.location
    working_makedefs_location = "%s/active/os/nuttx/Make.defs" % repo.location
    selected_makedefs_location = "%s/active/os/config/Make.defs" % repo.location

    if args.batch_names:

        batch_names = args.batch_names[::-1] if args.unbatch else args.batch_names

        batches = []
        for batch_name in batch_names:
            if batch_name[0] != '@':
                batches.append({'filename': batch_name, 'enable': True})
            else:
                batches.append({'filename': batch_name[1:], 'enable': False})    

        for batch in batches:
            batch_script_location = "%s/batches/%s.py" % (repo.location, batch['filename'])
            if not os.path.exists(batch_script_location):
                print()
                print(script.prefix("file [batches/%s] does not exist" % batch['filename']))
                exit()

        combined_batch_script = {}

        for batch in batches:
            batch_script_location = "%s/batches/%s.py" % (repo.location, batch['filename'])

            spec = importlib.util.spec_from_file_location("batch_script", batch_script_location)
            foo = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(foo)

            if batch['enable']:
                batch_script = foo.options['enable']
            else:
                batch_script = foo.options['disable']

            combined_batch_script.update(batch_script)

        print()

        repo.active_os.batch_config(combined_batch_script, selected_config_location)

        if args.test_batch:

            if args.unbatch:
                batch_script = foo.options['enable']
            else:
                batch_script = foo.options['disable']

            print()

            repo.active_os.batch_config(batch_script, selected_config_location)

    else:
        shutil.copyfile(working_config_location, generated_config_location)
        cmd_start_kconfig_frontends = ["bash", "-c", "KCONFIG_CONFIG='%s/active/os/nuttx/.config~' make %s" % (repo.location, repo.options['kconfig-frontend'])]
        subprocess.call(cmd_start_kconfig_frontends, cwd="%s/active/os/nuttx" % repo.location)

    cmd_compare_configs = 'diff %s %s -q' % (working_config_location, generated_config_location)
    cmd_compare_makedefs = 'diff %s %s -q' % (working_makedefs_location, selected_makedefs_location)

    config_matches = False
    if os.path.exists(working_config_location) and os.path.exists(generated_config_location):
        res_code = subprocess.call(cmd_compare_configs, shell=True)
        config_matches = (res_code == 0)

    if not config_matches:
        print()
        subprocess.call(["bash", "-c", "(which colordiff &> /dev/null) || diff %s %s && colordiff %s %s" % (working_config_location, generated_config_location, working_config_location, generated_config_location)])
        if args.no_confirm:
            shutil.copyfile(generated_config_location, working_config_location)
        else:
            script.copy_or_remove(generated_config_location, working_config_location)

    if args.batch_names and config_matches:
        print()
        if args.test_batch:
            print(script.prefix("Test successfull, applying a batch and then tearing it down reverts the changes"))
        else:
            print(script.prefix("Build was already configured with these options selected, nothing to change"))
        print()
