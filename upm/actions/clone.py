import os, shutil, subprocess

from upm.lib.Script import Script
script = Script()

# TODO clone to repo tmp not home tmp

def clone(repo, args):

    # Don't allow relative path as a name

    if args.os.find("/") != -1 or args.os.find("..") != -1:
        exit(script.prefix("the [name] cannot contain '/' nor '..'"))


    # Don't try to overwrite existing OS

    os_destination = "%s/oses/%s" % (repo.location, args.os)

    if os.path.exists(os_destination):
        print(script.prefix("os with this name already exists at the [%s] location" % os_destination))
        print(script.prefix("if the directory contain no NuttX OS remove it manually and try again"))
        exit()


    # Don't start cloning if remote repository does not exist

    # TODO: allow configuration of repo addresses template

    #~ repo_name = "%s-os" % args.os
    #~ remote_repo_location = "git@bitbucket.org:w8jcik/%s.git" % (repo_name)
    #~ print(script.prefix("trying to reach remote repo %s" % remote_repo_location))
    #~ cmd = "git ls-remote --exit-code -h %s" % remote_repo_location
    #~ res_code = subprocess.call(cmd.split())

    #~ if res_code != 2:

    #~ if res_code == 0:
    #~ exit(script.prefix("repository exists but is not empty"))
    #~ else:
    #~ exit(script.prefix("create remote repository [%s] or check access rights" % repo_name))

    clone_tmp_location = "%s/%s" % (repo.tmp_location, args.os)

    if os.path.exists(clone_tmp_location):
        print(script.prefix("temporary clone directory already exists"))
        script.removal(clone_tmp_location, essential=True)

    print(script.prefix("cloning into '%s'" % clone_tmp_location))

    cloning_failed = False

    res_code = 0

    try:
        #res_code = subprocess.call(["%s/tools/clone_nuttx.sh" % script.location, clone_tmp_location, remote_repo_location])
        res_code = subprocess.call(["%s/tools/clone_nuttx.sh" % script.location, clone_tmp_location, ""])
    except (KeyboardInterrupt, ProcessLookupError):
        cloning_failed = True
    finally:
        if cloning_failed:
            print()
            print(script.prefix("cloning interrupted!"))
            script.removal(clone_tmp_location)

    if res_code == 0:
        print(script.prefix("moving %s to %s" % (clone_tmp_location, os_destination)))
        shutil.move(clone_tmp_location, os_destination)
        #~ subprocess.call(["git", "submodule", "add", remote_repo_location, "oses/%s" % args.os], cwd="%s" % repo.location)
    else:
        script.removal(clone_tmp_location)

    repo.scan()
    repo.detect_issues()
