import os

from upm.lib.Repository import Repository

from upm.lib.Script import Script
script = Script()

templates = {
    "apps/Make.defs": """
include $(wildcard custom_apps/*/Make.defs)
""",
    "apps/Makefile": """
MENUDESC = "Custom apps"

include $(APPDIR)/Directory.mk
""",
    "libs/Make.defs": """
include $(wildcard custom_libs/*/Make.defs)
""",
    "libs/Makefile": """
MENUDESC = "Custom libraries"

include $(APPDIR)/Directory.mk
""",
}

def new(args):
    new_repo_location = os.path.abspath(args.target_directory)
    config_directory_location = os.path.join(new_repo_location, '.nuttx')

    already_initialized = False
    if os.path.exists(config_directory_location):
        print(script.prefix("existing nuttx repository detected"))
        already_initialized = True

    if already_initialized:
        if Repository.is_any_directory_missing(new_repo_location):
            print(script.prefix("do you want to create missing directories? (y, n, q)"), end=" ", flush=True)
            script.question_loop(y=(lambda: Repository.recreate_directories(new_repo_location)), n=quit)
        else:
            print(script.success('default directory structure is present'))
    else:
        Repository.recreate_directories(new_repo_location)

        for file, content in templates.items():
            path = "%s/%s" % (new_repo_location, file)
            if not os.path.exists(path):
                fp = open(path, "w")
                fp.write(content)
                fp.close()

        print()
        print(script.prefix("""as a next step, you might wish to download upstream NuttX now

    cd %s
    upm clone --os default
""" % new_repo_location))

    exit()
