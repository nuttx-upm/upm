import os, subprocess, glob

from upm.lib.Script import Script
script = Script()


def use(repo, args):
    if args.os:
        if args.os not in repo.os_names:
            print(script.failure("there is no operating system called [%s]" % args.os))
            exit()
        else:
            if repo.is_os_selected():
                repo.deselect_os()
            repo.select_os(args.os)
            repo.scan_os()

    if args.add_board:
        if args.add_board not in repo.boards:
            print(script.prefix("there is no board called [%s]" % args.add_board))
            exit()

        board_link_location = "%s/active/os/nuttx/configs/%s" % (repo.location, args.add_board)

        if os.path.exists(board_link_location):
            res_code = subprocess.call(["rm", board_link_location])
        res_code = subprocess.call(["ln", "-si", "../../../../boards/%s" % args.add_board, board_link_location])

    if args.remove_board:
        if args.remove_board not in repo.boards:
            print(script.prefix("there is no board called [%s], attempting to remove it anyway" % args.remove_lib))

        # TODO: some redundancy with add_board

        board_link_location = "%s/active/os/nuttx/configs/%s" % (repo.location, args.remove_board)

        if os.path.islink(board_link_location) or os.path.exists(board_link_location):
            res_code = subprocess.call(["rm", board_link_location])

    if args.add_board or args.remove_board:
        # TODO: handle dead links

        active_boards = []
        for possible_board in repo.boards:
            if os.path.islink("%s/active/os/nuttx/configs/%s" % (repo.location, possible_board)):
                active_boards.append(possible_board)

        # TODO: try catch to handle script failure
        cmd = ["%s/tools/add_boards_to_os.sh" % script.location, repo.location]
        for active_board in active_boards:
            cmd.append(active_board)
        res_code = subprocess.call(cmd)

    # TODO: this is redundant with the add_board/remove_board block
    # TODO: handle dead links

    active_boards = []
    for possible_board in repo.boards:
        if os.path.islink("%s/active/os/nuttx/configs/%s" % (repo.location, possible_board)):
            active_boards.append(possible_board)

    # TODO: should add if not added and possible to add

    if args.board:
        if args.board in active_boards:

            # TODO: this shouldn't print in the middle of printing status
            # TODO: should be executed only when args.board different then current board
            repo.clean()

            active_board_link_location = "%s/active/os/board" % repo.location

            if os.path.islink(active_board_link_location) or os.path.exists(active_board_link_location):
                res_code = subprocess.call(["rm", active_board_link_location])
            res_code = subprocess.call(["ln", "-si", "nuttx/configs/%s" % args.board, active_board_link_location])

            subprocess.call(["bash", "%s/board_deprecations/include_board_h.sh" % script.location], cwd=repo.location)
        else:
            print()
            if args.board in repo.boards:
                print(script.prefix("you have to link board [%s] first" % args.board))
            else:
                print(script.failure("there is no board called [%s]" % args.board))
            print()

    for possible_board in repo.boards:
        if os.path.islink("%s/active/os/nuttx/configs/%s" % (repo.location, possible_board)):

            active_board_link_location = "%s/active/os/board" % repo.location
            active_board_link_target = "%s/boards/%s" % (repo.location, possible_board)

            if os.path.exists(active_board_link_location) and os.path.islink(active_board_link_location) and os.path.exists(active_board_link_target) and os.path.samefile(active_board_link_location, active_board_link_target):
                # TODO: redundant with the one in the repo

                def list_subdir(subdir):
                    return [os.path.split(entry)[1]  for entry in glob.glob(os.path.join("%s/%s" % (repo.location, subdir), '*'))]

                def filter_entries(entries):
                    return [entry for entry in entries  if os.path.exists("%s/boards/%s/%s/defconfig" % (repo.location, possible_board, entry))]

                configs = filter_entries(list_subdir('boards/%s' % (possible_board)))

                active_config_link_location = "%s/active/os/config" % repo.location

                if 'config' in args and args.config:
                    if args.config in configs:

                        # TODO: execute only if args.config different than current configuration
                        repo.clean()

                        if os.path.islink(active_config_link_location) or os.path.exists(active_config_link_location):
                            res_code = subprocess.call(["rm", active_config_link_location])
                        res_code = subprocess.call(["ln", "-si", "board/%s" % args.config, active_config_link_location])