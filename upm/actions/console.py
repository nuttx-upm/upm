import os, socket, subprocess, time

from upm.lib.colors import colors

from upm.lib.Script import Script
script = Script()


def console(repo, args):
    print()

    if args.list == True:
        subprocess.call(['%s/tools/propose_serial_devices.sh' % script.location])
        print()
        exit()

    if args.telnet == True:

        if args.ip != None:
            telnet_ip = args.ip
        else:
            telnet_ip = subprocess.Popen(["%s/tools/get_ip.sh" % script.location, repo.location], stdout=subprocess.PIPE).communicate()[0].strip().decode()

        if len(telnet_ip) == 0:
            print(script.prefix('cannot determine ip addres, use --ip switch'))
            exit("")
        else:
            try:
                socket.inet_aton(telnet_ip)
            except socket.error:
                print(script.failure('given ip address is malformed'))
                exit("")

            subprocess.call(('telnet -e ^D %s' % telnet_ip).split())

    else:

        if args.dev != None:
            autoconnect = [args.dev]
        else:
            if repo.options['autoconnect'] != False:
                autoconnect = repo.options['autoconnect']
            else:
                autoconnect = []

        console_dev = subprocess.Popen(["%s/tools/get_uucp_dev.sh" % script.location] + autoconnect, stdout=subprocess.PIPE).communicate()[0].strip()

        if args.dev == None:
            if len(console_dev) == 0:
                print(script.prefix('cannot auto-detect correct serial device'))

        baud = 0

        if args.dev != None and not os.path.exists(args.dev):
            print(script.prefix("%sserial device %s%s%s doesn't exists%s" % (colors.FAIL, colors.ENDC, args.dev, colors.FAIL, colors.ENDC)))

        if len(console_dev) == 0 or not os.path.exists(console_dev):
            print()
            subprocess.call(['%s/tools/propose_serial_devices.sh' % script.location])
            print()
            print(script.prefix('use --dev switch to select device from above, for example --dev /dev/ttyS0'))
            exit("")

        # TODO: warn about missing terminal application (screen)

        if args.baud != None:
            baud = args.baud;
        else:
            baud = int(subprocess.Popen(["%s/tools/get_baud.sh" % script.location, repo.location], stdout=subprocess.PIPE).communicate()[0].strip())

            if baud == 0:
                print(script.prefix('cannot determine the baud rate, use --baud switch'))
                exit("")

        if baud not in [300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 28800, 38400, 57600, 115200]:
            print(script.prefix('selected baud rate %d is non standard' % baud))
            time.sleep(3)

        subprocess.call(['screen', console_dev, str(baud)])
