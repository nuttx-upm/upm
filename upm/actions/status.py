import os, subprocess, glob

from upm.lib.colors import colors

from upm.lib.Script import Script
script = Script()


def list_oses(self):

    print("Operating systems")

    if len(self.oses) == 0:
        print("  - empty -")

    for possible_os in self.oses:
        os_version = possible_os.get_os_version()

        if self.active_os != None and possible_os.name == self.active_os.name:
            print(" %s*%-20s %s" % (colors.OKGREEN, possible_os.name, os_version), colors.ENDC)
        else:
            print("  %-20s %s" % (possible_os.name, os_version))

    print()


def list_libs(self):

    print("Libraries")

    if self.are_external_libs_linked():
        if len(self.libs) > 0:
          for lib in self.libs:
              lib_active = False
              if os.path.exists("%s/active/os/nuttx/.config" % self.location):
                  res_code = subprocess.call('grep "CONFIG_CUSTOM_LIB_%s=y" %s/active/os/nuttx/.config &> /dev/null' % (lib.upper(), self.location), shell=True)
                  lib_active = (res_code == 0)

              if lib_active:
                  print(" %s*%s" % (colors.OKGREEN, lib), colors.ENDC)
              else:
                  print("  %s" % lib)
        else:
            print("  (empty)")
    else:
        print("  (empty)")

    print()


def list_apps(self):

    print("Applications")

    if self.are_external_apps_linked():
        if len(self.apps) > 0:
          for app in self.apps:
              app_active = False
              if os.path.exists("%s/active/os/nuttx/.config" % self.location):
                  res_code = subprocess.call('grep "CONFIG_CUSTOM_APP_%s=y" %s/active/os/nuttx/.config &> /dev/null' % (app.upper(), self.location), shell=True)
                  app_active = (res_code == 0)

              if app_active:
                  print(" %s*%s" % (colors.OKGREEN, app), colors.ENDC)
              else:
                  print("  %s" % app)
        else:
            print("  (empty)")
    else:
        print("  (empty)")

    print()


def status(repo):
    print()
    list_oses(repo)
    list_libs(repo)
    list_apps(repo)

    print("Boards")
    board_labels = []

    if len(repo.boards) > 0:
      for possible_board in repo.boards:
          if os.path.islink("%s/active/os/nuttx/configs/%s" % (repo.location, possible_board)):
              active_board_link_location = "%s/active/os/board" % repo.location
              active_board_link_target = "%s/boards/%s" % (repo.location, possible_board)

              if os.path.exists(active_board_link_location) and os.path.islink(active_board_link_location) and os.path.exists(active_board_link_target) and os.path.samefile(active_board_link_location, active_board_link_target):
                  print(" %s*%s" % (colors.OKGREEN, possible_board), colors.ENDC)

                  label = subprocess.Popen(["%s/tools/standard_name.sh" % script.location], stdout=subprocess.PIPE, cwd=active_board_link_target).communicate()[0].strip().decode()
                  if len(label) > 0:
                      board_labels.append("%s %s" % (possible_board, label))

                  # TODO: redundant with the one in the repo

                  def list_subdir(subdir):
                      return [os.path.split(entry)[1] for entry in glob.glob(os.path.join("%s/%s" % (repo.location, subdir), '*'))]

                  def filter_entries(entries):
                      return [entry for entry in entries if os.path.exists("%s/boards/%s/%s/defconfig" % (repo.location, possible_board, entry))]

                  configs = filter_entries(list_subdir('boards/%s' % (possible_board)))

                  active_config_link_location = "%s/active/os/config" % repo.location

                  print()
                  print("  Configurations")
                  for config in configs:
                      active_config_link_target = "%s/boards/%s/%s" % (repo.location, possible_board, config)

                      if os.path.exists(active_config_link_location) and os.path.exists(active_config_link_target) and os.path.samefile(active_config_link_location, active_config_link_target):
                          print("   %s*%s" % (colors.OKGREEN, config), colors.ENDC)
                      else:
                          print("    %s" % config)

                      # TODO: signal that Make.defs differs

                  print()

              else:
                  print("  %s%s" % (colors.OKGREEN, possible_board), colors.ENDC)
          else:
              print("  %s" % possible_board)
    else:
        print("  (empty)")

    print()

    # boards_label = ""
    #
    # if len(board_labels) > 0:
    #     boards_label = "%s" % ", ".join(board_labels)
    #
    # print("Package name: \"%s\"" % repo.describe(boards_label))

    # TODO: what if files are missing

    repo.detect_issues()

    # How does it work
    # Creates links to custom boards:
    # $ ln -s ../../../../boards/some_board active/os/nuttx/configs/some_board
