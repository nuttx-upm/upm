import subprocess

from upm.lib.Script import Script
script = Script()


def diff(repo, args):
    if args.compare_config:

        active_config_location   = "%s/active/os/config/defconfig" % repo.location
        current_config_location  = "%s/active/os/nuttx/.config"    % repo.location
        active_makedef_location  = "%s/active/os/config/Make.defs" % repo.location
        current_makedef_location = "%s/active/os/nuttx/Make.defs"  % repo.location

        subprocess.call(["meld", "--diff", active_config_location, current_config_location, "--diff", active_makedef_location, current_makedef_location])

    if args.compare_os:
        subprocess.call(["bash", "-c", "cd %s/active/os/ ; git difftool --dir-diff vanilla" % repo.location])
