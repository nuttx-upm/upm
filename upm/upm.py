#!/usr/bin/env python3

import os, glob
import multiprocessing, math

from upm.lib.Script import Script
script = Script()

import argparse, subprocess

parser = argparse.ArgumentParser()

# TODO those arguments are not simple nor modular

subparsers = parser.add_subparsers(dest="action")

init_parser = subparsers.add_parser('new', help="initialize empty repository", aliases=["init"])
init_parser.add_argument("target_directory", help="directory name", nargs="?", const=1, default=os.getcwd())

clone_parser = subparsers.add_parser('clone', help='clone new OS')
clone_group = clone_parser.add_mutually_exclusive_group(required=True)
clone_group.add_argument("--os", dest="os", default="default", metavar='NAME', help='make a fresh clone of NuttX')

run_parser = subparsers.add_parser('run', help='compile and flash active configuration')
run_parser.add_argument("--jobs", dest="jobs", default=(math.ceil(1.5 * multiprocessing.cpu_count())), type=int)
run_parser.add_argument("--list", dest="list", action="store_true")
run_parser.add_argument("--skip-build", "-s", dest="skip_build", action="store_true")
run_parser.add_argument("--for-distribution", "-d", dest="for_distribution", action="store_true")
run_parser.add_argument("--loader", dest="loader")
run_parser.add_argument("--dev", dest="dev")

clean_parser = subparsers.add_parser('clean', help='remove binary files')
clean_parser.add_argument("--apps", dest="apps", action="store_true")

status_parser = subparsers.add_parser('status')

use_parser = subparsers.add_parser('use', help='link and select active components')
use_group = use_parser.add_mutually_exclusive_group(required=True)
use_group.add_argument("--add-board", dest="add_board", metavar='BOARD', help='attach board')
use_group.add_argument("--remove-board", dest="remove_board", metavar='BOARD', help='detach board')
use_group.add_argument("--os", dest="os", help='change os')
use_group.add_argument("--board", dest="board", help='change board')
use_group.add_argument("--config", dest="config", help='change configuration')

diff_parser = subparsers.add_parser('diff', help='analyse differences in code and configurations')
diff_group = diff_parser.add_mutually_exclusive_group(required=True)
diff_group.add_argument("--compare-config", dest="compare_config", action="store_true", help="compare active board config with the saved one")
diff_group.add_argument("--compare-os", dest="compare_os", action="store_true", help="compare active os with its vanilla version")

config_parser = subparsers.add_parser('config', help='configure board')
config_parser.add_argument("--startup", dest="startup", action="store_true", help='edit the init script')
config_parser.add_argument("--link-apps", dest="link_apps", action="store_true", help='recreates link to repo apps inside of active OS')
config_parser.add_argument("--batch", dest="batch_names", nargs="*", help='change multiple Kconfig options')
config_parser.add_argument("--unbatch", "-u", dest="unbatch", action="store_true", help='change multiple Kconfig options, use undo variant')
config_parser.add_argument("--test-batch", "-t", dest="test_batch", action="store_true", help='tests reversibility of a batch by applying it and then tearing it down')
config_parser.add_argument("--no-confirm", "-y", dest="no_confirm", action="store_true", help='don\'t ask for confirmation')

dist_parser = subparsers.add_parser('dist', help='copies binary to [dist] with a timestamp')

def reject_empty_str(value):
    if len(value) == 0:
        raise argparse.ArgumentTypeError("%s is empty string" % value)
    return value

generator_parser = subparsers.add_parser("generate", help="can generate app or config from blueprints", aliases=['g'])
generator_parser.add_argument("--app", dest="app", help="name of your new app", type=reject_empty_str)
generator_parser.add_argument("--board", dest="board", help="name of your new board")
generator_parser.add_argument("--app-blueprint", dest="app_blueprint", help="blueprint name", type=reject_empty_str)
generator_parser.add_argument("--board-blueprint", dest="board_blueprint", help="board blueprint name")
generator_parser.add_argument("--os-version-file", dest="os_version_file", action="store_true", help="generate .version file inside active os")

console_parser = subparsers.add_parser('console', help='attepmts connection to the nsh console')
console_parser.add_argument("--telnet", dest="telnet", action="store_true")
console_parser.add_argument("--list", "-l", dest="list", action="store_true")
# TODO: should be mutually exclusive with telnet
console_parser.add_argument("--baud", dest="baud", type=int)
console_parser.add_argument("--ip", dest="ip")
console_parser.add_argument("--dev", dest="dev")

args = parser.parse_args()

if args.action is None:
    parser.print_help()
    exit()

# TODO: allow skip of this sanity check, reset skip config every change of compiler, introduce state along config
res_code = subprocess.call(["%s/tools/sanity_check.sh" % script.location])

import upm.actions

if args.action in ['init', 'new']:
    upm.actions.new(args)

from upm.lib.Repository import Repository

try:
    repo = Repository(os.getcwd())
except FileExistsError as e:
    exit(script.prefix("%s" % e))

if args.action == 'clean':
    upm.actions.clean(repo, args)

if args.action == 'diff':
    upm.actions.diff(repo, args)

if args.action == 'run':
    upm.actions.run(repo, args)

if args.action == 'config':
    upm.actions.config(repo, args)

if args.action in ['init', 'new', 'clone']:
    upm.actions.clone(repo, args)

if args.action in ['generate', 'g']:
    upm.actions.generate(repo, args)

if args.action == 'console':
    upm.actions.console(repo, args)

if args.action == 'dist':
    upm.actions.dist(repo, args)

if args.action == 'use':
    upm.actions.use(repo, args)

if args.action == 'status':
    upm.actions.status(repo)
