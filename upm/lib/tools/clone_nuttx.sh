#!/bin/bash

# Context-safe effect
# Runs from any place

# TODO: not argument safe

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source ${DIR}/try-helper.bash
source ${DIR}/color-helper.bash

if [ $# -eq 0 ]; then
  echo "${0##*/} location [repo name]"
  exit 1
fi

location=$1

if [[ ! -e "${location}" ]]; then
  mkdir "${location}" -p
  
  location=$(realpath $location)
  name=$(basename $location)
  
  cd "${location}"

  try git clone https://bitbucket.org/nuttx/apps.git apps
  try git clone https://bitbucket.org/nuttx/nuttx.git nuttx

  cd "${location}/apps"
  try git submodule update --init --recursive
  ln -s ../../../apps custom_apps
  ln -s ../../../libs custom_libs

  cd "${location}/nuttx"
  try git submodule update --init --recursive
else
  echo "${0##*/}: Some files are already present at the ${location}"
  exit 1
fi
