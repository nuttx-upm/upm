#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source ${DIR}/color-helper.bash

# Display a hint if no arguments provided

if [ $# -lt 1 ]; then
  echo "${0##*/} repo_location"
  exit 1
fi

repo_location="$1"
config_location="${repo_location}/active/os/nuttx/.config"

report=""

if [ -e "${config_location}" ]; then
  ip=$(egrep CONFIG_NSH_IPADDR=0x[a-f0-9\.]* "${config_location}" | egrep -o [a-f0-9\.]{2} | while read line ; do printf "%d\n" "0x${line}" ; done | paste -sd "." - )
  
  if [ ! -z "${ip}" ]; then
    report="${ip}"
  fi
fi

echo "${report}"
