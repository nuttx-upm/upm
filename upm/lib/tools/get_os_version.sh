#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source ${DIR}/try-helper.bash
source ${DIR}/color-helper.bash

if [ $# -eq 0 ]; then
  echo "${0##*/} os_location"
  exit 1
fi

location=$1

version=""

if [ -e "${location}/nuttx/.version" ]; then
  source "${location}/nuttx/.version"
  echo "${CONFIG_VERSION_STRING}"
fi
