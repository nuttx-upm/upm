#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source ${DIR}/color-helper.bash

# Display a hint if no arguments provided

if [ $# -lt 1 ]; then
  echo "${0##*/} repo_location"
  exit 1
fi

repo_location="$1"
config_location="${repo_location}/active/os/nuttx/.config"

report="0"

if [ -e "${config_location}" ]; then
  no=$(grep CONFIG_USART[0-9]_SERIAL_CONSOLE=y "${config_location}" | grep -o [0-9])

  if [ ! -z "${no}" ]; then
    baud=$(grep CONFIG_USART${no}_BAUD "${config_location}" | grep -o [0-9]* | tail -n 1 | head -n 1)
    
    if [ ! -z "${baud}" ]; then
      report="${baud}"
    fi
  fi
fi

echo "${report}"
