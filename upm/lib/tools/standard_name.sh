#!/bin/sh

if git describe --tags --always &>/dev/null ; then
  name="`git describe --tags --always`"
  if ! git diff-index --quiet HEAD --; then
    name="${name}-uncommited"
  fi
else
  name="unversioned"
fi

echo "${name}"
