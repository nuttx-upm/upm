#!/bin/bash

echo "known serial devices":

echo ""

function get_driver_name() {
  echo $(basename $(readlink -f "/sys/class/tty/${1/\/dev\//}/device/driver"))
}

ls /dev/tty* -al | egrep -e "uucp|dialout" | egrep -o "/dev/ttyS[0-9]*" | while read line ; do printf "  %-13s %-11s\n" "${line}" $(get_driver_name "${line}") ; done | GREP_COLOR="1;32" grep --color "/dev/\S*"

for sysdevpath in $(find /sys/bus/usb/devices/usb*/ -name dev); do
  syspath="${sysdevpath%/dev}"
  devname="$(udevadm info -q name -p $syspath)"

  [[ "$devname" == "bus/"* ]] && continue
  eval "$(udevadm info -q property --export -p $syspath)"
  [[ -z "$ID_SERIAL" ]] && continue

  if [ $(stat --format=%G "/dev/$devname") == 'uucp' ]; then
    uid="${ID_VENDOR_ID}:${ID_MODEL_ID}"
    if [ "${ID_USB_INTERFACE_NUM}" -ne 0 ]; then
      uid="${uid}.${ID_USB_INTERFACE_NUM}"
    fi

    printf "  %-13s %-11s %-13s %s" "/dev/${devname}" $(get_driver_name "/dev/${devname}") "${uid}" "${ID_SERIAL//_/ }"
  fi

  echo
done | GREP_COLOR="1;32" egrep --color -e "/dev/\S*|[a-z0-9]*:[a-z0-9]*.?[0-9]*"
