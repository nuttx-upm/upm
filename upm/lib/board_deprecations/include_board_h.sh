if [ -e "active/os/board/include/board.h" ]; then
  cat "active/os/board/include/board.h" | grep "stm32_boardinitialize(" > /dev/null
  
  if [ $? -eq 0 ]; then
    echo
    echo "  Deprecation Warning:"
    echo "    * unnecessary stm32_boardinitialize in include/board.h"
    echo "    * http://bitbucket.org/nuttx/nuttx/commits/0f10f6bdec9875e27c2031ee2e7c67824caaace1"
    echo
  else

    cat "active/os/board/include/board.h" | grep "_boardinitialize(" > /dev/null
    if [ $? -eq 0 ]; then
      echo
      echo "  Deprecation Warning:"
      echo "    * unnecessary {arch}_boardinitialize in include/board.h"
      echo "    * https://bitbucket.org/nuttx/nuttx/commits/a1e250697cc8540c47c81b8ea376a998e5a9796c"
      echo
    fi
    
  fi
fi
