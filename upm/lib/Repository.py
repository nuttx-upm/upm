import glob, json, os, subprocess, datetime
from os.path import expanduser

from .OS import OS
from .Script import Script
script = Script()

repo_related_directories = ['.nuttx', 'active', 'apps', 'boards', 'dist', 'libs', 'oses', 'tmp']

class Repository:

    # Label generation

    def get_os_label(self):

        os_label = ""
        humanized_version = ""
        if self.active_os != None:
            humanized_version = self.active_os.get_humanized_version()
            if humanized_version != "":
                os_label = "%s %s" % (self.active_os.name, humanized_version)

        return os_label

    def get_lib_labels(self):

        lib_labels = []

        if self.are_external_libs_linked():
            for lib in self.libs:
                res_code = subprocess.call('grep "CONFIG_CUSTOM_LIB_%s=y" %s/active/os/nuttx/.config &> /dev/null' % (lib.upper(), self.location), shell=True)
                if res_code == 0:
                    label = subprocess.Popen(["%s/tools/standard_name.sh" % script.location], stdout=subprocess.PIPE, cwd="%s/libs/%s" % (self.location, lib)).communicate()[0].strip().decode()
                    if len(label) > 0:
                        lib_labels.append("%s %s" % (lib, label))

        return lib_labels

    def get_app_labels(self):

        app_labels = []

        if self.are_external_apps_linked():
            for app in self.apps:
                res_code = subprocess.call('grep "CONFIG_CUSTOM_APP_%s=y" %s/active/os/nuttx/.config &> /dev/null' % (app.upper(), self.location), shell=True)
                if res_code == 0:
                    label = subprocess.Popen(["%s/tools/standard_name.sh" % script.location], stdout=subprocess.PIPE, cwd="%s/apps/%s" % (self.location, app)).communicate()[0].strip().decode()
                    if len(label) > 0:
                        app_labels.append("%s %s" % (app, label))

        return app_labels

    def describe(self, boards_label):

        os_label = self.get_os_label()

        date_file_label = datetime.datetime.utcnow().strftime("%y%m%d")
        date_label = datetime.datetime.utcnow().strftime("%y%m%d%H%M")

        lib_labels = self.get_lib_labels()
        libs_label = ", ".join(lib_labels)

        app_labels = self.get_app_labels()
        apps_label = ", ".join(app_labels)

        cmd = ["%s/tools/add_version_strings.sh" % script.location, self.location, date_label, apps_label, boards_label, libs_label, os_label]
        subprocess.call(cmd)

        labels = []
        if len(apps_label) > 0:
            labels.append(apps_label)

        if len(boards_label) > 0:
            labels.append("board " + boards_label)

        if len(lib_labels) > 0:
            if len(lib_labels) == 1:
                labels.append("lib " + libs_label)
            else:
                labels.append("libs " + libs_label)

        labels.append("os " + os_label)

        # TODO: could create patches for "modified" components

        other_labels = ", ".join(labels)
        return "%s - %s" % (date_file_label, other_labels)

    def clean(self):
        try:
            res_code = subprocess.call([
                "bash",
                "-c",
                "make --silent --no-print-directory clean ; make --silent --no-print-directory subdir_distclean"
            ], cwd="%s/active/os/nuttx" % self.location)
            if res_code != 0:
                exit(script.prefix("cleaning failed!"))
        except (KeyboardInterrupt, ProcessLookupError):
            print()
            exit(script.prefix("cleaning interrupted!"))

    def is_any_directory_missing(new_repo_location):
        for d in repo_related_directories:
            if not os.path.isdir(os.path.join(new_repo_location, d)):
                return True
        return False

    def recreate_directories(new_repo_location):
        created = []
        skipped = []

        for d in repo_related_directories:
            target_directory = os.path.join(new_repo_location, d)

            if os.path.exists(target_directory):
                if os.path.isdir(target_directory):
                    skipped.append(d)
                else:
                    print(script.failure('%s exists but is not a directory, delete it and try again' % d))
            else:
                created.append(d)
                subprocess.call(['mkdir', target_directory, '-p'])

        if len(skipped) > 0:
            print(script.warning('skipping %s' % ', '.join(skipped)))

        if len(created) > 0:
            # TODO: should prefix with directory name if it was provided
            print(script.success('created %s' % ', '.join(created)))

    # Link apps to active OS

    def external_apps_link_location(self):
        return os.path.join(self.location, 'active/os/apps/custom_apps')

    def external_libs_link_location(self):
        return os.path.join(self.location, 'active/os/apps/custom_libs')

    def are_external_apps_linked(self):
        apps_link_location = self.external_apps_link_location()

        return os.path.exists(apps_link_location)\
            and os.path.islink(apps_link_location)\
            and os.path.realpath(apps_link_location) == os.path.realpath(os.path.join(self.location, 'apps'))

    def are_external_libs_linked(self):
        libs_link_location = self.external_libs_link_location()

        return os.path.exists(libs_link_location)\
            and os.path.islink(libs_link_location)\
            and os.path.realpath(libs_link_location) == os.path.realpath(os.path.join(self.location, 'libs'))

    def link_external_apps(self):
        if not self.are_external_apps_linked():
            subprocess.call(['ln', '-s', '../../../apps', self.external_apps_link_location()])

    def link_external_libs(self):
        if not self.are_external_libs_linked():
            subprocess.call(['ln', '-s', '../../../libs', self.external_libs_link_location()])

    # OS is selected

    def get_active_os_path(self):
        return os.path.join(self.location, 'active/os')

    def deselect_os(self):
        subprocess.call(["rm", "%s/active/os" % self.location])

    def selected_os_is_dead_link(self):
        return not os.path.exists(os.path.realpath(self.get_active_os_path()))

    def is_os_selected(self):
        # TODO: should check if the link is not a dead one
        return os.path.exists(self.get_active_os_path())

    def select_os(self, name):
        subprocess.call(["ln", "-si", "../oses/%s" % name, "%s/active/os" % self.location])

    # Libraries

    def get_lib_link_location(self, lib):
        return "%s/active/os/nuttx/%s" % (self.location, lib.name)

    def add_lib(self, lib):
        lib_link_location = self.get_lib_link_location(lib)

        if os.path.exists(lib_link_location):
            subprocess.call(["rm", lib_link_location])
        subprocess.call(["ln", "-si", "../../../libs/%s" % lib.name, lib_link_location])

    def remove_lib(self, lib):
        lib_link_location = self.get_lib_link_location(lib)

        # islink is required in case of dead links, exists in case of directories
        if os.path.islink(lib_link_location) or os.path.exists(lib_link_location):
            subprocess.call(["rm", lib_link_location])

    # Preserve downloaded

    def save_downloaded(self):
        filename = "v5.0.2.tar.gz"
        downloaded = "%s/apps/graphics/littlevgl/%s" % (self.get_active_os_path(), filename)
        temporary = "%s/%s" % (self.tmp_location, filename)
        if os.path.exists(downloaded):
            subprocess.call(["cp", downloaded, temporary])

    def restore_downloaded(self):
        filename = "v5.0.2.tar.gz"
        downloaded = "%s/apps/graphics/littlevgl/%s" % (self.get_active_os_path(), filename)
        temporary = "%s/%s" % (self.tmp_location, filename)
        if os.path.exists(temporary):
            subprocess.call(["cp", temporary, downloaded])

    # Issue detection

    def detect_issues(self):

        issues_found = 0

        if self.active_os == None:
            if len(self.oses) > 0:
                print()
                print(script.prefix('none of the oses is selected'))
                print(script.success('nuttx use --os [%s]' % '|'.join([os.name for os in self.oses])))
                # TODO: nuttx use --os should display list
                issues_found += 1
            else:
                print()
                print(script.prefix('you should clone nuttx os and apps bundle'))
                print(script.success('nuttx clone --os [name]'))
                issues_found += 1
        elif self.selected_os_is_dead_link():
            print()
            print(script.failure('path %s is a broken symlink' % self.get_active_os_path()))
            issues_found += 1
        else:
            if not self.are_external_apps_linked():
                print()
                print(script.prefix('you might want to link your apps to the active os'))
                print(script.success('nuttx config --link-apps'))
                issues_found += 1

        if issues_found > 0:
            print()

    # List content of the repo

    def scan_os(self):
        if not os.path.exists(self.get_active_os_path()) or self.selected_os_is_dead_link():
            self.active_os = None
        else:
            self.active_os = OS(os.path.realpath(os.path.join(self.location, 'active/os')))

    def scan(self):
        """Repository detection"""

        # TODO application could track repo location, gcc version to advise recompilation

        def list_subdir(subdir):
            return [os.path.split(entry)[1]  for entry in glob.glob(os.path.join(self.location, subdir, '*'))]

        self.boards = list_subdir('boards')
        self.apps = [app for app in list_subdir('apps')  if os.path.isdir(os.path.join(self.location, "apps", app))]
        self.os_names = list_subdir('oses')
        self.libs = [lib for lib in list_subdir('libs')  if os.path.isdir(os.path.join(self.location, "libs", lib))]

        self.boards.sort()
        self.apps.sort()
        self.os_names.sort()
        self.libs.sort()

        self.oses = [OS(os.path.join(self.location, "oses", name))  for name in self.os_names]

        # TODO: only if os selected, and directory present
        # TODO there should be an interface for such predicates ! DUPLICATE

        self.blueprints.apps = [app for app in list_subdir('active/os/apps/examples')  if os.path.isdir(os.path.join("%s/active/os/apps/examples/%s" % (self.location, app)))]
        self.blueprints.apps.sort()

        self.blueprints.boards = [board for board in list_subdir('active/os/nuttx/configs')  if os.path.isdir(os.path.join("%s/active/os/nuttx/configs/%s" % (self.location, board)))]
        self.blueprints.boards.sort()

        from upm import default
        self.options = default.options

        # TODO: should extend not override

        for options_location in ["%s/.nuttx-cli.json" % expanduser("~"), "%s/nuttx.json" % self.location]:

            if os.path.exists(options_location):
                with open(options_location) as data:
                    try:
                        self.options.update(json.load(data))
                    except json.decoder.JSONDecodeError:
                        print()
                        print(script.failure("cannot parse %s" % options_location))

    def __init__(self, path):
        found = False
        while path != '/':
            if '.nuttx' in os.listdir(path):
                found = True
                break
            path = os.path.split(path)[0]

        self.blueprints = type('obj', (object,), {'apps': [], 'boards': []})

        if found:
            self.location = path
            self.tmp_location = "%s/tmp" % self.location
            self.scan()
            self.scan_os()
        else:
            raise FileExistsError('not a Nuttx repo')
