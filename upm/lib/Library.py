import os

from .Script import Script
script = Script()


class Library:

    def __init__(self, path):
        self.location = os.path.realpath(path)
        self.name = self.location.split("/")[-1]
