import os, sys, readchar, shutil

from .colors import colors

class Script:
    def __init__(self):
        self.name = os.path.split(sys.argv[0])[1]
        self.location = os.path.split(os.path.realpath(__file__))[0]
        self.tmp_dir = "%s/tmp/%s" % (os.environ['HOME'], self.name)

    def prefix(self, msg):
        return "%s: %s" % (self.name, msg)

    def failure(self, msg):
        return "%s: %s%s%s" % (self.name, colors.FAIL, msg, colors.ENDC)

    def success(self, msg):
        return "%s: %s%s%s" % (self.name, colors.OKGREEN, msg, colors.ENDC)

    def warning(self, msg):
        return "%s: %s%s%s" % (self.name, colors.WARNING, msg, colors.ENDC)

    def question_loop(self, y=None, n=None, q=exit):
        ch = '-'
        while ch not in ['y', 'n', 'q']:
            ch = readchar.readchar()

        print(ch)

        if ch == 'q':
            q()

        if ch == 'n':
            n()

        if ch == 'y':
            y()

    def removal(self, path, essential=False):

        def removal_confirmed():
            shutil.rmtree(path)
            print(self.prefix("directory removed successfully"))

        print(self.prefix("do you want to remove %s? (y, n, q)" % path), end=" ", flush=True)

        if essential:
            def refusal_cb():
                print(self.prefix("terminating"))
                exit()
        else:
            def refusal_cb():
                print(self.prefix("skipping"))

        self.question_loop(y=removal_confirmed, n=refusal_cb)

    def copy_or_remove(self, source_path, target_path):

        def accepted():
            shutil.copyfile(source_path, target_path)
            print(self.prefix("config altered"))

        def rejected():
            os.remove(source_path)
            print(self.prefix("removing temporary config"))

        print(self.prefix("do you accept the changes (y, n, q)?  — rollback otherwise"))
        self.question_loop(y=accepted, n=rejected)

    def backup_or_restore(self, path, backup_path):

        def backup_confirmed():
            shutil.copyfile(path, backup_path)
            print(self.prefix("config saved"))

        def rollback():
            shutil.copyfile(backup_path, path)
            print(self.prefix("rolling back"))

        print(self.prefix("do you accept the changes (y, n, q)?  — rollback otherwise"))
        #~ print(self.prefix("will copy %s to %s, rollback otherwise." % (path, backup_path)))

        self.question_loop(y=backup_confirmed, n=rollback)

    def colprint(self, iterable, width=80):

        if len(iterable) == 0:
            return

        def columnify(iterable, margin=0):
            strings = [x for x in iterable]
            widest = max(len(x) for x in strings)
            padded = [x.ljust(widest+margin) for x in strings]
            return padded

        columns = columnify(iterable, margin=4)
        colwidth = len(columns[0])+2
        perline = (width-4) // colwidth
        for i, column in enumerate(columns):
            print("  ", end='')
            print(column, end='')
            if i % perline == perline-1:
                print('\n', end='')
        print()
