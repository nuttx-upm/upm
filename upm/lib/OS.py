import os, subprocess

from .Script import Script
script = Script()

from .colors import colors
from .Kconfiglib import kconfiglib

class OS:

    def __init__(self, path):
        self.location = os.path.realpath(path)
        self.name = self.location.split("/")[-1]

    def get_os_version(self):
        return subprocess.Popen(["%s/tools/get_os_version.sh" % script.location, self.location], stdout=subprocess.PIPE).communicate()[0].strip().decode()

    def get_humanized_version(self):
        return subprocess.Popen(["%s/tools/standard_name.sh" % script.location], stdout=subprocess.PIPE, cwd=self.location).communicate()[0].strip().decode()

    def generate_version_file(self):
        subprocess.call(["%s/tools/generate_version_file.sh" % script.location, self.location])

    def build_nuttx_configs_dummy_Kconfig(self, jobs = 1):

        build_failed = False

        try:
            res_code = subprocess.call(["bash", "-c", "time make depend --jobs %d" % jobs], cwd="%s/nuttx" % self.location)
        except (KeyboardInterrupt, ProcessLookupError):
            build_failed = True
        finally:
            if build_failed:
                print()
                exit(script.prefix("bulid interrupted!"))

        if res_code != 0:
            exit(script.prefix("build failed!"))

        print()

    def build_apps_Kconfig(self, jobs = 1):

        build_failed = False

        try:
            res_code = subprocess.call(["bash", "-c", "time make apps_preconfig --jobs %d" % jobs], cwd="%s/nuttx" % self.location)
        except (KeyboardInterrupt, ProcessLookupError):
            build_failed = True
        finally:
            if build_failed:
                print()
                exit(script.prefix("bulid interrupted!"))

        if res_code != 0:
            exit(script.prefix("build failed!"))

        print()

    def save_current_config_header(self):

        nuttx_dir = "%s/nuttx" % self.location
        retrieve_header_cmd = ["bash", "-c", "head -n 4 .config > .config.header"]
        subprocess.call(retrieve_header_cmd, cwd=nuttx_dir)

    def restore_header(self):

        nuttx_dir = "%s/nuttx" % self.location
        restore_header_cmd = ["bash", "-c", "mv .config~ .config.gen ; mv .config.header .config~ ; cat .config.gen >> .config~ ; rm .config.gen"]
        subprocess.call(restore_header_cmd, cwd=nuttx_dir)

    def batch_config(self, options, selected_config_location):

        nuttx_dir = "%s/nuttx" % self.location
        apps_dir = "%s/apps" % self.location

        appsdir_var_was_already_set = ('APPSDIR' in os.environ)

        if not appsdir_var_was_already_set:
            os.environ["APPSDIR"] = apps_dir

        if not os.path.exists("%s/Kconfig" % apps_dir):
            self.build_apps_Kconfig()

        if not os.path.exists("%s/configs/dummy/Kconfig" % nuttx_dir):
            self.build_nuttx_configs_dummy_Kconfig()

        try:
            conf = kconfiglib.Config(("%s/Kconfig" % nuttx_dir), base_dir=nuttx_dir)
            conf.load_config(selected_config_location)
        finally:
            if not appsdir_var_was_already_set:
                del os.environ["APPSDIR"]


        max_key_length = 0
        max_prompt_length = 0

        for key, value in options.items():

            key_length = len(key)

            if (key_length > max_key_length):
                max_key_length = key_length
                
            prompt = conf[key].get_prompts()[0]
            prompt_length = len(prompt)
            
            if (prompt_length > max_prompt_length):
                max_prompt_length = prompt_length

        for key, value in options.items():

            prompt = conf[key].get_prompts()[0]

            color = colors.OKGREEN
            if value == 'n':
                color = colors.FAIL

            line_format = "%%s%%-%ss%%s %%-%ss set to %%s%%s%%s" % (max_key_length + 1, max_prompt_length + 1)

            print(script.prefix(line_format % (color, key, colors.ENDC, prompt, color, value, colors.ENDC)))
            conf[key].set_user_value(value)

        self.save_current_config_header()

        conf.write_config(("%s/.config~" % nuttx_dir))

        self.restore_header()
