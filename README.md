# upm – NuttX project manager

Separates your applications and your configurations from the vanilla NuttX codebase

![status](upm/docs/tree.png)

Allows fast switching between different versions of the OS

![status](upm/docs/status.png)

Includes a generator of new applications that treats `apps/examples` as blueprints

![status](upm/docs/blueprint.png)

Ships with a few macros that makes daily work with NuttX faster and less prone to human-error

![status](upm/docs/help.png)

# Basics

```
$ upm init [repo_name]         # create a directory structure with empty folders [apps], [boards] and [oses]
```

then anywhere inside the repo

```
$ upm new [os_name]            # clone latest nuttx from bitbucket, place it in [oses]
$ upm generate                 # create a new app treating [apps/examples] as blueprints, place it in [apps]
  --app [name]
  --app-blueprint [example]
$ upm config                   # invoke the configuration menu
$ upm run                      # compile the firmware and load it into a microcontroller
$ upm clean                    # clean the build of all generated files
$ upm console                  # start serial console
$ upm console --telnet         # start telnet console
```

* The same app can be compiled with multiple oses
    * different versions
    * with different set of customizations
    * it makes updates and debugging easier
* External libraries can be attached and detached with automatic code injection

```
$ upm status                   # list apps, oses, configs and libs
$ upm use --os [name]          # select OS
$ upm use --board [name]       # select board
$ upm use --config [name]      # select configuration
```

# Installation

* `pip install --user git+https://gitlab.com/w8jcik/upm.git`
* Add `PATH=$PATH:$HOME/.local/bin` to your `.bashrc` if your `~/.local/bin` is not in the path. (restart terminal to make it work)

# Prerequisites

* Python 3.x
* [Git](https://git-scm.com/)
* GCC 4.x crosscompiler for *arm-none-eabi* target
* [kconfig-frontends](http://ymorin.is-a-geek.org/projects/kconfig-frontends) kconfig-frontends
* awk, bash, diff, egrep, find, grep, perl, sed, udevadm (systemd), nano, which
* [screen](http://www.gnu.org/software/screen/) (optional, for serial terminal)
* [inetutils](http://www.gnu.org/software/inetutils/) (optional, for telnet terminal)
* [colordiff](http://www.colordiff.org/) (optional)
* [Meld](http://meldmerge.org/) (optional)

## Installation of prerequisites

* `# apt install python3 python3-pip genromfs screen inetutils-telnet colordiff meld` (Ubuntu)
* `# pacman -S screen inetutils colordiff meld`, `$ aurman -S genromfs kconfig-frontends` (Arch Linux)

## Kconfig frontends on Ubuntu

* `# add-apt-repository ppa:george-hopkins/kconfig-frontends`
* `# apt-get update`
* `# apt install kconfig-ncurses`

## Compiler

Either use https://bitbucket.org/nuttx/buildroot or packages from your distribution 

* `# apt install gcc-arm-none-eabi-gcc` (Ubuntu)
* `# pacman -S arm-none-eabi-gcc arm-none-eabi-binutils` (Arch Linux)

For Arch there is also an AUR package https://gitlab.com/w8jcik/nuttx-aur which wrapps https://bitbucket.org/nuttx/buildroot.

# Building and running NuttX

To prepare the firmware and load it into the microcontroller
```
$ upm run
```
You can be anywhere in your repo.

After successfull build, loader program is selected basing on the USB devices present in your system. Additional loading programs can be configured by analogy to the [two predefined ones](upm/default.py#default.py-28).

![devices](upm/docs/loaders.png)

## USB loaders

Configuration of a new USB loader requires you to provide a shell command and a USB ID

`~/.nuttx-cli.json` or `nuttx.json`
```
{
  "loaders": {
    "stlink": {
      "usb": ["0483:3748"],
      "command": "st-flash write '%binary%' 0x08000000"
    }
  }
}
```
Within the command you can use variables `%binary%` and `%hex%`. They are locations of binary and hex firmware.

You can get the correct USB ID with `lsusb`

![lsusb](upm/docs/lsusb.png)

## Generic serial loaders

Generic STM32 loader `stmflasher` can use any serial converter to load firmware. Because of this, it defaults to an empty list of matching devices. You have to provide correct device name in your configuration file.


`~/.nuttx-cli.json` or `nuttx.json`
```
{
  "loaders": {
    "stmflasher": {
      "serial": ["067b:2303"]
    }
  }
}
```

Alternatively you can specify a device from the command line, at each run
```
$ upm run --loader stmflasher --dev /dev/ttyS0
```

You will get the list of serial devices with `nuttx console --list`.

![devices](upm/docs/devices.png)

In case of serial devices you can use both types of device names, for example `067b:2303` as well as `/dev/ttyUSB0`. The more readable names like `/dev/ttyUSB[0-9]*` and `/dev/ttyACM[0-9]*` sometimes get reordered so it is safer to use USB IDs.

## Config

To configure the OS
```
$ upm config
```
You can be anywhere in your repo.

[By default](upm/default.py) it starts ncurses frontend

![config_kconfig](upm/docs/kconfig.png)

After exiting you have an opportunity to review and rollback changes

![config_confirm](upm/docs/config_confirm.png)

To get the diff in color install `colordiff`.

## Clean

The building system of NuttX is not always 100% successfull in determining what should be rebuilt when you are changing things. If you are getting suspicious that something misbehave you can try to clean all generated files
```
$ upm clean
```
then build again
```
$ upm run
```

# Connection with NSH console

## Serial

To connect over serial
```
$ upm console
```
Baud rate is sniffed from the current OS configuration. Can be specified with `--baud` option.

Tool uses `screen` terminal emulator which might be difficult to exit. You have to press `Ctrl + a, k, y`.

### Default serial device

Most common USB-to-serial adapters are [picked up automatically](upm/default.py#default.py-2). Serial device can be also specified using `--dev` option

```
$ upm console --dev /dev/ttyS0
```

You can get correct `--dev` using `--list`

![devices](upm/docs/devices.png)

Default serial device can be overriden with a configuration file.

`~/.nuttx-cli.json` or `nuttx.json`
```
{
  ...
  "autoconnect": ["067b:2303"]                # connect only to "Prolific USB-Serial Controller", skip other vendors
  ...
  "autoconnect": ["/dev/ttyS0"]               # connect to the first internal port
  ...
  "autoconnect": ["067b:2303", "/dev/ttyS0"]  # in case "067b:2303" is not present, connect to "/dev/ttyS0"
  ...
  "autoconnect": false                        # you have to use --dev switch every time
  ...
}
```

## Telnet

To connect over telnet
```
$ upm console --telnet
```
IP adress is sniffed from the current OS configuration. Can be specified with `--ip` option.

Tool uses `telnet` command from `inetutils` package. Press `Ctrl + d` to exit.
