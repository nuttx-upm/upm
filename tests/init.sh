# Run from root dir with `bash tests/init.sh`

set -e


echo "Clean test workdir"

rm tmp -rf

# Presence of directories

function check_dirs_presence() {
    [[ -e tmp/.nuttx ]]
    [[ -e tmp/active ]]
    [[ -e tmp/apps ]]
    [[ -e tmp/boards ]]
    [[ -e tmp/dist ]]
    [[ -e tmp/libs ]]
    [[ -e tmp/oses ]]

    return 0
}


echo "Create directory structure from the outside"

upm init tmp

check_dirs_presence


echo "Clean test workdir"

rm tmp -rf


echo "Create directory structure from the inside"

( mkdir -p tmp && cd tmp && upm init . )

check_dirs_presence
