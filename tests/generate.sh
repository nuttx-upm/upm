# Run from root dir with `bash tests/generate.sh`

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


echo "Setup fake environment"

example="active/os/apps/examples/hello"

rm tmp -rf
upm init tmp

mkdir -p "tmp/active/os"
mkdir -p "tmp/${example}/"
rsync -a "${DIR}/generate/${example}/" "tmp/${example}/"


echo "Generate application"

( cd tmp && upm generate --app custom_hello --app-blueprint hello )

[[ -e tmp/apps/custom_hello ]]

grep custom_apps tmp/apps/Make.defs > /dev/null
